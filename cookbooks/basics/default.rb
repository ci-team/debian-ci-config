include_recipe "lvm_merged"

if node['swap']
  loc = node['swap']['location'] ? node['swap']['location'] : '/swap'
  size = node['swap']['size']

  execute 'setup swap file' do
    command "dd if=/dev/zero of=#{loc} bs=1M count=#{size}000 && chmod 0600 #{loc} && mkswap #{loc} && swapon #{loc}"
    not_if "test -f #{loc}"
  end

  execute 'setup swap on boot' do
    command "echo '#{loc}  none swap  sw  0 0' >> /etc/fstab"
    not_if "grep -q #{loc} /etc/fstab"
  end
else
  execute 'setup swap file' do
    command "dd if=/dev/zero of=/swap bs=512K count=$(free -m | awk '/^Mem:/{print $2}') && chmod 0600 /swap && mkswap /swap && swapon /swap"
    not_if 'systemd-detect-virt --container --quiet || grep -q swap /etc/fstab || test -f /swap'
  end

  execute 'setup swap on boot' do
    command 'echo "/swap  none swap  sw  0 0" >> /etc/fstab'
    not_if 'systemd-detect-virt --container --quiet || grep -q swap /etc/fstab'
  end
end

if node['tmp_on_tmpfs']
  execute 'setting up tmp on tmpfs and mount' do
    command 'echo "tmpfs /tmp tmpfs rw,nr_inodes=2M,size=%sG 0 0" >> /etc/fstab' % node['tmp_on_tmpfs']
    not_if 'grep -q " /tmp " /etc/fstab'
  end
end

if node['use_apt_cacher']
  package 'apt-cacher-ng'
#  package 'auto-apt-proxy'
end

if node['apt-proxy']
  template '/etc/apt/apt.conf.d/02proxy' do
    owner  'root'
    group  'root'
    mode   '0644'
  end
end

remote_file "/srv/authorized_keys" do
  owner   'root'
  group   'root'
  mode    "0644"
  notifies :run, 'execute[copy authorized_keys]'
end

package 'sudo'
execute 'copy authorized_keys' do
  action :nothing
  command 'sudo -u ${SUDO_USER:-$USER} sh -c "mkdir -p ~/.ssh && chmod 700 ~/.ssh && cp /srv/authorized_keys ~/.ssh && chmod 600 ~/.ssh/authorized_keys"'
end

template '/etc/apt/sources.list' do
  variables mirror: node['mirror']
  notifies :run, 'execute[apt-get update]', :immediately
  owner   'root'
  group   'root'
  mode    "0644"
end

execute 'apt-get update' do
  action :nothing
end

remote_file '/etc/apt/apt.conf.d/00updates' do
  source 'files/apt.conf'
  owner   'root'
  group   'root'
  mode    "0644"
end

file '/etc/cloud/cloud.cfg.d/99_ci_debian_net.cfg' do
  content [
    'preserve_hostname: true',
    'manage_etc_hosts: false',
  ].join("\n")
  only_if "test -f /etc/cloud/cloud.cfg"
end

remote_file '/etc/modprobe.d/block-virtio_gpu.conf' do
  owner   'root'
  group   'root'
  mode    "0644"
  only_if 'test -f /etc/modprobe.d'
end

file '/etc/timezone' do
  content "Etc/UTC\n"
  owner   'root'
  group   'root'
  mode    "0644"
end

link '/etc/localtime'  do
  to '/usr/share/zoneinfo/Etc/UTC'
  force true
end

remote_file '/etc/apt/apt.conf.d/52unattended-upgrades-local' do
  owner   'root'
  group   'root'
  mode    '0644'
end
package 'unattended-upgrades'

# useful tools
package 'ack'
package 'bash-completion'
package 'chrony'
package 'emacs-nox'
package 'fail2ban'
package 'htop'
package 'iotop-c'
package 'iptraf-ng'
package 'kitty-terminfo'
package 'local-apt-repository'
package 'ncdu'
package 'net-tools'
package 'pastebinit'
package 'systemd-cron'
package 'telnet'
package 'tmux'
package 'vim'
package 'virt-what'
directory '/srv/local-apt-repository' do
  owner   'root'
  group   'root'
  mode    "0755"
end
