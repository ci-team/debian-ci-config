desc "Prints server inventory"
task :inventory do
  header = ["Host", "Runs", "Location", "tmpfs", "swap", "Hardware"]
  data = [header]
  Chake.nodes.each do |node|
    arch = node.data["debci"]["arch"]
    arch ||= File.basename(node.data["chake_metadata"]["definition_file"], ".yaml")
    location = node.data.dig("metadata", "location")
    tmp_on_tmpfs = node.data["tmp_on_tmpfs"]
    swap = node.data.dig("swap", "size")
    hardware = node.data.dig("metadata", "hardware")
    data << [node.hostname, arch, location, tmp_on_tmpfs, swap, hardware]
  end
  IO.popen(["column", "--table", "--separator=\t"], "w") do |table|
    data.each do |row|
      table.puts(row.join("\t"))
    end
  end
end
