require_program "git"

desc 'Deploys and creates a tag'
task :deploy do
  if $ENV == 'production'
    sh 'git tag --sign $(date --utc +%Y-%m-%d_%H-%M-$USER)'
    sh 'git push --tags'
  end
  Rake::Task[:upgrade].invoke
end
