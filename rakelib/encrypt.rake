require_program 'wipe'
require_program 'gpg'

GPG_RECIPIENTS = [
  'B2DEE66036C40829FCD0F10CFC0DB1BBCD460BDE', # terceiro
  '58B66D48736BE93B052DE6729C5C99EB05BD750A', # elbrus
].map { |k| "--recipient #{k}" }.join(" ")

def decrypt(*files)
  files.each do |f|
    sh "gpg --use-agent --quiet --decrypt --output #{f} #{f}.asc"
  end
  begin
    yield
  ensure
    files.each do |f|
      sh "wipe -f -s #{f}"
    end
  end
end

def encrypt(*files)
  files.each do |f|
    sh "gpg --use-agent --quiet --armor --encrypt --trust-model always #{GPG_RECIPIENTS} --output #{f}.asc #{f}"
    fail("Cannot encrypt #{f}!") unless File.exist?(f)
    sh "wipe -f -s #{f}"
  end
end

desc "Encrypts file"
task :encrypt, :file do |task, args|
  encrypt args[:file]
end
