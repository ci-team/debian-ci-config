begin
  skip_list = File.readlines("nodes.ignore").map(&:strip).reject do |line|
    line.empty? || line =~ /^#/
  end.map(&:split).map(&:first)
rescue Errno::ENOENT
  skip_list = []
end

skipped = $nodes.select do |node|
  skip_list.include?(node.hostname)
end
skipped.each do |node|
  puts "I: ignoring #{node.hostname}, listed in nodes.ignore"
  regex = /:#{node.hostname}$/
  Rake::Task.tasks.each do |task|
    task.prerequisites.reject! { |name| name =~ regex }
  end
  Rake::Task.tasks.reject! { |task| task.name =~ regex }
end

$active_nodes = $nodes - skipped

$master_node = $nodes.find do |node|
  node.data.values_at('itamae', 'itamae-remote').flatten.include?('roles/ci_master.rb')
end
$workers = $active_nodes.select do |node|
  node.data.values_at('itamae', 'itamae-remote').flatten.include?('roles/ci_worker.rb')
end
