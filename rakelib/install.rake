$DEBS_DIR = File.absolute_path(File.join(ENV["CHAKE_TMPDIR"], 'debs'))

desc 'Uploads build debci binaries'
multitask 'debci:copy' => $active_nodes.map { |node| "debci:copy:#{node.hostname}"}

desc 'Build debci tree at $FROM (default: ../debci)'
task 'debci:build', :to do |_, args|
  require_program "debchange", "devscripts"
  require_program "dpkg-parsechangelog", "dpkg-dev"
  require_program "gbp", "git-buildpackage"
  require_program "git"

  source = ENV.fetch('FROM', '../debci')
  v = nil
  chdir source do
    debs = []
    v0 = `dpkg-parsechangelog -SVersion`.strip
    v = `git describe --tags | tr - +`.strip
    if v == v0
      debs = `debc --list-debs`.split
    end
    if debs.empty? || v != v0
      origbranch = `git symbolic-ref --short HEAD`.strip
      sh "git switch -c snapshot-#{v}"
      sh "debchange -v #{v} Snapshot"
      sh 'debcommit -a'
      debs = `debc --list-debs`.split
      if debs.empty?
        sh 'DEB_BUILD_OPTIONS=nocheck gbp buildpackage --git-ignore-branch'
        debs = `debc --list-debs`.split
      end
      sh "git checkout #{origbranch}"
      sh "git branch -D snapshot-#{v}"
    end
    mkdir_p $DEBS_DIR
    rm_f Dir[$DEBS_DIR + '/*.deb']
    cp debs, $DEBS_DIR
  end
  $VERSIONS["debci"] = ENV["DEBCI_VERSION"] = v
  set_versions
end

def marker(node)
  install_dir = File.absolute_path(File.join(ENV["CHAKE_TMPDIR"], 'install'))
  FileUtils.mkdir_p(install_dir)
  File.join(install_dir, node.hostname + "_" + $VERSIONS["debci"])
end

$active_nodes.each do |node|
  desc "Uploads built debci binaries to #{node.hostname}"
  multitask "debci:copy:#{node.hostname}" => "debci:build" do
    next if File.exist?(marker(node))
    dest = "#{node.username}@#{node.hostname}:/var/tmp/debs/"
    sh *node.rsync, "-avp", "--delete", "#{$DEBS_DIR}/", dest
  end

  desc 'Builds and uploads debci tree at $FROM (default: ../debci)'
  multitask "debci:upload:#{node.hostname}" => "debci:copy:#{node.hostname}" do |_, args|
    next if File.exist?(marker(node))
    v = $VERSIONS["debci"]
    Rake::Task["run:#{node.hostname}"].invoke("sudo cp /var/tmp/debs/*.deb /srv/local-apt-repository/ && while ! grep -q '^Version: #{v}$' /var/lib/local-apt-repository/Packages; do sleep 1; done && sudo apt-get update")
  end

  desc 'Installs debci from source tree at $FROM (default: ../debci)'
  multitask "install:#{node.hostname}" => "debci:upload:#{node.hostname}" do |_, args|
    m = marker(node)
    if File.exist?(m)
      puts "I: Skipping #{node.hostname}, already done"
    else
      Rake::Task["converge:#{node.hostname}"].invoke
      touch(m)
    end
  end
end

desc 'Install debci binaries'
multitask 'install' => $active_nodes.map { |node| "install:#{node.hostname}"}
