package 'apache2'
execute 'a2dissite 000-default.conf' do
  only_if 'test -f /etc/apache2/sites-enabled/000-default.conf'
end
