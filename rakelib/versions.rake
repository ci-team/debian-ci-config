$VERSIONS = {
  "debci"         => ENV.fetch('DEBCI_VERSION', '3.12'),
  "autodep8"      => ENV.fetch('AUTODEP8_VERSION', '0.28+nmu1'),
  'autopkgtest'   => ENV.fetch('AUTOPKGTEST_VERSION', '5.46'),
}

def set_versions
  $nodes.each do |node|
    node.data['versions'] = $VERSIONS.dup
  end
end

set_versions
