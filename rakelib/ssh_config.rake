unless Chake::VERSION >= '0.15'
  fail "chake version 0.15 or higher required (you have #{Chake::VERSION})"
end

node_files = Dir.glob("#{ENV["CHAKE_NODES"]}") + Dir.glob(ENV["CHAKE_NODES_D"] + '/*.yaml')


task Chake.tmpdir do |t|
  mkdir_p t.name
end

if $nodes.map { |node| node.data['public_ip'] || node.data['ssh_options'] }.compact.size > 0
  ssh_config = File.join('config', $ENV, 'ssh_config')
  ENV['CHAKE_SSH_CONFIG'] = ssh_config
  task :connect_common => ssh_config
  file ssh_config => node_files + [Chake.tmpdir] + [:load_ips] do |t|
    require 'erb'
    template = File.read("cookbooks/munin/templates/ssh_config.erb")
    erb = ERB.new(template, trim_mode: '-')
    @nodes = $nodes.first.data['hosts']
    @user_known_hosts_file = "#{Chake.tmpdir}/known_hosts"
    config = erb.result
    File.open(ssh_config, 'w') do |f|
      f.write(config)
    end
    puts "#{t.name} ← #{t.prerequisites.join(', ')}" unless Rake.application.options.silent
  end
  file ssh_config => Dir[".vagrant/machines/*/*/id"] if $ENV == "development"
  desc 'Update SSH client configuration'
  task :ssh_config => ssh_config

  proxies = []
  $nodes.each do |node|
    options =node.data['ssh_options']
    next unless options
    next unless options['ProxyJump']

    proxy = options['ProxyJump']
    task "connect:#{node.hostname}" => "connect:#{proxy}"

    next if proxies.include?(proxy)
    proxies << proxy
  end

  proxies.each do |proxy|
    desc "sets up connection to #{proxy}"
    task "connect:#{proxy}" do
      sh 'ssh', '-F', ssh_config, proxy, 'true'
    end
  end
end
