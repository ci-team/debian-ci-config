desc 'Upgrade all hosts (only CI components)'
task :upgrade do
  Rake::Task['run'].invoke('sudo apt-get update')
  Rake::Task['converge'].invoke
end

desc 'apt autoremove and autoclean all hosts'
task :apt_remove_and_clean do
  Rake::Task['run'].invoke('sudo apt-get autoremove -y && sudo apt-get autoclean > /dev/null')
end

desc 'Runs `apt-get dist-upgrade` on all hosts'
task :dist_upgrade do
  Rake::Task['run'].invoke('sudo apt-get update && sudo DEBIAN_FRONTEND=noninteractive apt-get -qy -o Dpkg::Options::=--force-confdef -o Dpkg::Options::=--force-confold dist-upgrade')
  Rake::Task['converge'].invoke
end

desc 'Upgrades a release on all hosts (`debian_release` in nodes configuration needs updating first)'
task :release_upgrade do
  Rake::Task["apply"].invoke("cookbooks/basics/default.rb") # setup sources.list
  Rake::Task[:dist_upgrade].invoke
end
