desc 'run a command on all workers'
multitask 'run:workers' => $workers.map { |w| "run:#{w.hostname}" }

$native_arch = `dpkg --print-architecture`.strip

def get_arch(node)
  arch = node.data["debci"]["arch"]
  arch = $native_arch if (arch == "native")
  arch ||= node.hostname.sub(/ci-worker-(.*)-\d+/,  '\1')
  arch = "amd64" if arch =~ /^ci-worker/
  arch
end

namespace :workers do
  desc 'stop all workers'
  task :stop do
    Rake::Task[:run_input].invoke("sudo systemctl stop debci-worker@* ; sleep 5 ; sudo systemctl stop debci-worker")
    Rake::Task['run:workers'].invoke
  end

  desc 'start all workers'
  task :start do
    Rake::Task[:run_input].invoke("sudo systemctl start debci-worker")
    Rake::Task['run:workers'].invoke
  end

  desc 'get worker status'
  task :status do
    Rake::Task[:run_input].invoke("sudo sh -c 'for c in $(lxc-ls --running); do printf \"$c\\t\"; cat /var/lib/lxc/$c/rootfs/var/tmp/debci.pkg 2>/dev/null || echo; done'")
    Rake::Task["run:workers"].invoke
  end

  desc 'show age of autopkgtest-* lxc containers'
  task :lxc_age do
    Rake::Task[:run_input].invoke("sudo sh -c 'for c in $(lxc-ls --filter autopkgtest-*); do TZ=UTC ls -ald /var/lib/lxc/$c ; done'")
    Rake::Task["run:workers"].invoke
  end

  desc 'show age of new autopkgtest lxc container'
  task :lxc_age_new do
    Rake::Task[:run_input].invoke("sudo sh -c 'for c in $(lxc-ls --filter autopkgtest-*-new); do TZ=UTC ls -ald /var/lib/lxc/$c ; done'")
    Rake::Task["run:workers"].invoke
  end

  desc 'show age of ci-* lxc containers'
  task :lxc_age_ci do
    Rake::Task[:run_input].invoke("sudo sh -c 'for c in $(lxc-ls --filter ci-*); do TZ=UTC ls -ald /var/lib/lxc/$c ; done'")
    Rake::Task["run:workers"].invoke
  end

  desc 'show how many debci-workers each node has'
  task :capacity do
    $workers.each do |node|
      number = node.data["parallel_jobs"] || 1
      printf "%-20s\t%2d\n", node.hostname, number
     end
  end

  desc 'Runs `apt-get dist-upgrade` on all workers'
  task :dist_upgrade do
    Rake::Task[:run_input].invoke('sudo apt-get update && sudo DEBIAN_FRONTEND=noninteractive apt-get -qy -o Dpkg::Options::=--force-confdef -o Dpkg::Options::=--force-confold dist-upgrade')
    Rake::Task["run:workers"].invoke
  end

  desc 'List nodes vs architecture'
  task :list do
    $workers.each do |node|
      puts [node.hostname, get_arch(node)].join("\t")
    end
  end
end

workers_by_arch = $workers.group_by { |node| get_arch(node) }

desc 'show how many debci-workers each arch has'
task :capacity do
  workers_by_arch.each do |arch, nodes|
    count = nodes.map { |n| n.data["parallel_jobs"] || 1 }.sum
    printf "%-10s\t%2d\n", arch, count
  end
end

workers_by_arch.each do |arch, nodes|
  desc "run a command on #{arch} nodes"
  multitask "run:#{arch}" => nodes.map { |w| "run:#{w.hostname}" }

  desc "convert all #{arch} hosts"
  multitask "converge:#{arch}" => nodes.map { |w| "converge:#{w.hostname}" }

  desc "install locally-built debci to #{arch} hosts"
  multitask "install:#{arch}" => nodes.map { |w| "install:#{w.hostname}" }

  desc "check connection to #{arch} hosts"
  multitask "check:#{arch}" => nodes.map { |w| "check:#{w.hostname}" }
end

desc 'restarts ci-worker-armel-01 VM'
task :armel_01_restart do
  Rake::Task[:run_input].invoke("virsh destroy ci-runner-arm-01 && virsh start ci-runner-arm-01")
  Rake::Task["run:altra"].invoke
end
