task :psql do
  target = [$master_node.username, $master_node.hostname].join("@")
  config = ENV["CHAKE_SSH_CONFIG"]
  sh 'ssh', '-F', config, '-t', target, 'sudo', '-u', 'debci', 'psql'
end
