package 'munin'

file '/etc/munin/munin.conf' do
  content "includedir /etc/munin/munin-conf.d\n"
  owner   'root'
  group   'root'
  mode    '0644'
end

template '/etc/munin/munin-conf.d/hosts.conf' do
  owner   'root'
  group   'root'
  mode    '0644'
end

directory '/var/lib/munin' do
  owner   'munin'
  group   'www-data'
  mode    '0755'
end

directory '/var/lib/munin/.ssh' do
  owner   'munin'
  group   'munin'
  mode    '0700'
end

template '/var/lib/munin/.ssh/config' do
  source 'templates/ssh_config.erb'
  variables nodes: node['hosts']
  owner 'munin'
  group 'munin'
  mode  '0600'
end

remote_file '/var/lib/munin/.ssh/id_ed25519' do
  sensitive true
  source  "files/host-#{node["fqdn"]}/munin"
  owner   'munin'
  group   'munin'
  mode    '0600'
end

remote_file '/var/lib/munin/.ssh/id_ed25519.pub' do
  source  "files/munin@#{node["fqdn"]}.pub"
  owner   'munin'
  group   'munin'
  mode    '0600'
end
