package 'autodep8' do
  action :install
  version node['versions']['autodep8']
end
package 'autopkgtest' do
  action :install
  version node['versions']['autopkgtest']
end

package 'lxc'
package 'lxc-templates'

template '/etc/lxc/default.conf' do
  source  'templates/lxc.conf.erb'
end

service 'lxc-net' do
  action :nothing
end

remote_file '/etc/default/lxc-net' do
  source   "files/lxc-net"
  owner    "root"
  group    "root"
  mode     "0644"
  notifies :restart, 'service[lxc-net]'
end

v = node["versions"]["debci"]

package 'debci-worker' do
  action :install
  version v
  not_if "dpkg --compare-versions $(dpkg-query --show debci-worker | awk '{print($2)}') ge #{v}"
end
service 'debci-worker' do
  action :nothing
end

if node['tmp_on_tmpfs']
  remote_file '/usr/local/bin/copy-lxc-to-tmp-and-mount' do
    owner   'root'
    group   'root'
    mode    "0644"
  end

  remote_file '/etc/systemd/system/var-lib-lxc-on-tmpfs.service' do
    owner   'root'
    group   'root'
    mode    "0644"
  end

  execute 'copy debci-worker@.service to /etc and tune' do
    commands = [
      'cp -p /usr/lib/systemd/system/debci-worker@.service /etc/systemd/system/',
      'sed --in-place "s/\(After=.*\)/\1 var-lib-lxc-on-tmpfs.service/" /etc/systemd/system/debci-worker@.service',
    ]
    command commands.join(' && ')
    not_if 'test -f /etc/systemd/system/debci-worker@.service'
  end

end

if node['debci']['amqp_server']
  [
    "/etc/debci/ca.crt",
    "/etc/debci/#{node["fqdn"]}.crt",
    "/etc/debci/#{node["fqdn"]}.key",
  ].each do |f|
    remote_file f do
      sensitive true
      source    "../rabbitmq/files/host-#{node[:fqdn]}/#{File.basename(f)}"
      owner     'root'
      group     'debci'
      mode      '0440'
    end
  end
  template '/etc/debci/conf.d/00_amqp_server.conf' do
    owner     'root'
    group     'debci'
    mode      '0440'
  end
end

if node['debci']['backends'] && node['debci']['backends'].include?('qemu')
  package "vmdb2"
  package "zerofree"
  # TODO: replace "qemu-system" with the line below once we run trixie
  # package ["qemu-system-", node['debci']['arch']].join
  package "qemu-system"
  execute "adduser debci kvm"
  directory "/etc/systemd/system/debci-worker@1.service.d"
  file '/etc/systemd/system/debci-worker@1.service.d/qemu.conf' do
    content [
      "[Service]",
      "ExecStart=",
      "ExecStart=/usr/share/debci/bin/debci worker --backend=qemu",
    ].join("\n")  + "\n"
    owner "root"
    group "root"
    mode  "0644"
  end
end

file '/etc/debci/conf.d/00_mirror.conf' do
  content [
    "export debci_mirror=#{node['mirror']}\n",
    "export MIRROR=#{node['mirror']}\n",
  ].join
  owner "root"
  group "root"
  mode  "0644"
end

if node['debci'] && node['debci']['autopkgtest-options']
  file '/etc/debci/conf.d/00_autopkgtest_options.conf' do
    content [
      "export debci_autopkgtest_args=\"#{node['debci']['autopkgtest-options'].join(' ')}\"\n",
    ].join
    owner "root"
    group "root"
    mode  "0644"
  end
end

if node['debci'] && node['debci']['autopkgtest-options-lxc']
  file '/etc/debci/conf.d/00_autopkgtest_options_lxc.conf' do
    content [
      "export debci_autopkgtest_args_lxc=\"#{node['debci']['autopkgtest-options-lxc'].join(' ')}\"\n",
    ].join
    owner "root"
    group "root"
    mode  "0644"
  end
end

if node['debci'] && node['debci']['autopkgtest-options-qemu']
  file '/etc/debci/conf.d/00_autopkgtest_options_qemu.conf' do
    content [
      "export debci_autopkgtest_args_qemu=\"#{node['debci']['autopkgtest-options-qemu'].join(' ')}\"\n",
    ].join
    owner "root"
    group "root"
    mode  "0644"
  end
end

if node['parallel_jobs']
  jobs = Integer(node['parallel_jobs'])
  (2..jobs).each do |i|
    execute "systemctl enable debci-worker@#{i}" do
      not_if "systemctl is-enabled debci-worker@#{i}"
    end
    execute "systemctl start debci-worker@#{i}" do
      not_if "systemctl is-active debci-worker@#{i} || ! systemctl is-active debci-worker"
    end
  end
end

include_recipe "on_debci_conf_change"
on_debci_conf_change 'debci-worker' do
  action :restart
end
on_debci_conf_change 'debci-publisher' do
  action :restart
end

%w[
  debci_packages_processed
  debci_packages_being_processed
  debci_containers
].each do |plugin|
  link "/etc/munin/plugins/#{plugin}" do
    to "/usr/share/munin/plugins/#{plugin}"
    force true
    user 'root'
    notifies :restart, 'service[munin-node]'
  end
end

remote_file '/etc/cron.daily/cleanup-leftover-containers' do
  owner     'root'
  group     'root'
  mode      '0755'
end

remote_file '/usr/local/bin/prevent-disk-filling' do
  owner     'root'
  group     'root'
  mode      '0755'
end

remote_file '/etc/cron.d/prevent-disk-filling_cron_d' do
  owner     'root'
  group     'root'
  mode      '0644'
end
