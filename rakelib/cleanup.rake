desc 'stop worker, destroy all stopped lxc''s that were left behind and reboot'
task :cleanup do
  cmd = 'systemctl stop debci-worker@* && sleep 90 && for c in $(lxc-ls --stopped --filter=ci-); do chattr -R -i /var/lib/lxc/$c/rootfs/ 2>/dev/null || true; lxc-destroy -n $c 2>&1; done && reboot'
  Rake::Task[:run_input].invoke("sudo sh -c '%s'" % cmd)
  Rake::Task['run:workers'].invoke
end

