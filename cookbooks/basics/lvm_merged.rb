if node['lvm_merged']
  package "lvm2"
  execute 'setup lvm' do
    not_if 'grep -q /mnt/lxc-containers /etc/fstab'
    commands = [
      'vgcreate vg_lxc %s' % node['lvm_merged']['pvs'].join(' '),
      'modprobe dm-mod',
      'lvcreate --type striped --name lxc-containers --extents 100%VG vg_lxc',
      'mkfs.ext4 /dev/vg_lxc/lxc-containers',
      'mkdir -p /mnt/lxc-containers',
      'mount /dev/vg_lxc/lxc-containers /mnt/lxc-containers',
      'echo "/dev/vg_lxc/lxc-containers /mnt/lxc-containers ext4 defaults 0 0" >> /etc/fstab',
    ]
    command commands.join(' && ')
  end
end
