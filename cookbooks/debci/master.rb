if node['web_hostname']
  file "/etc/debci/conf.d/00_url_base.conf" do
    content "debci_url_base=https://%s\n" % node['web_hostname']
    owner     'root'
    group     'root'
    mode      '0644'
  end
end

if node['debci'] && node['debci']['architectures']
  file '/etc/debci/conf.d/00_arch_list.conf' do
    content "debci_arch_list='%s'\n" % node['debci']['architectures'].join(' ')
    owner     'root'
    group     'root'
    mode      '0644'
  end
end

package 'postgresql'
package 'ruby-pg'
package 'ruby-localhost' if node['debci']['development']
service 'postgresql'

total_memory = node[:ram]
shared_buffers = total_memory / 5        # 20%
effective_cache_size = total_memory / 5  # 20%

execute "pg_conftool set shared_buffers #{shared_buffers}MB && pg_conftool set effective_cache_size #{effective_cache_size}MB && systemctl restart postgresql" do
  only_if "test \"$(pg_conftool show --short shared_buffers)\" != #{shared_buffers}MB || test \"$(pg_conftool show --short effective_cache_size)\" != #{effective_cache_size}MB"
end

execute 'createuser' do
  user 'postgres'
  command 'createuser debci || true'
  not_if %q[test $(psql postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='debci'") = 1]
end

execute 'createdb' do
  user 'postgres'
  command 'createdb --owner=debci debci || true'
  not_if %q[test $(psql postgres -tAc "SELECT 1 FROM pg_database where datname = 'debci'") = 1]
end

file '/etc/debci/conf.d/00_database.conf' do
  content "debci_database_url='postgresql:///debci'"
  owner     'root'
  group     'root'
  mode      '0644'
end

file '/etc/debci/conf.d/00_data_retention.conf' do
  content "debci_data_retention_days=60\n"
  owner     'root'
  group     'root'
  mode      '0644'
end

file '/etc/debci/conf.d/00_slow_tests_duration.conf' do
  content "debci_slow_tests_duration_minutes=150\n"
  owner     'root'
  group     'root'
  mode      '0644'
end

remote_file '/etc/debci/reject_list' do
  notifies :reload, 'service[apache2]'
  owner     'root'
  group     'root'
  mode      '0644'
end

template '/etc/debci/extra_apt_sources_list.yaml' do
  notifies :restart, 'service[apache2]'
  source 'templates/extra_apt_sources_list.yaml.erb'
  owner 'root'
  group 'root'
  mode  '0644'
end

package "libdbd-pg-perl"

%w[
  debci_queue_size
  debci_total_packages_processed
  postgres_autovacuum
  postgres_bgwriter
  postgres_checkpoints
  postgres_connections_db
  postgres_users
  postgres_xlog
  postgres_cache_
  postgres_connections_
  postgres_locks_
  postgres_oldest_prepared_xact_
  postgres_prepared_xacts_
  postgres_querylength_
  postgres_scans_
  postgres_size_
  postgres_streaming_
  postgres_transactions_
  postgres_tuples_
].each do |src|
  dst = (src =~ /_$/) ? "#{src}debci" : src
  link "/etc/munin/plugins/#{dst}" do
    to "/usr/share/munin/plugins/#{src}"
    force true
    user      'root'
    notifies :restart, 'service[munin-node]'
  end
end

v = node["versions"]["debci"]

package 'debci-collector' do
  action :install
  version v
  notifies :reload, 'service[apache2]'
  not_if "dpkg --compare-versions $(dpkg-query --show debci-collector | awk '{print($2)}') ge #{v}"
end


secret_key_file = '/etc/debci/conf.d/00_session_secret.conf'
file secret_key_file do
  not_if "test -f #{secret_key_file}"
  sensitive true
  random =  SecureRandom.hex(64)
  content   "debci_session_secret='#{random}'\n"
  owner      'nobody'
  group     'debci'
  mode      '0440'
end

salsa_conf = "files/host-#{node['fqdn']}/salsa.conf"
if File.exist?(File.dirname(__FILE__) + "/" + salsa_conf)
  remote_file '/etc/debci/conf.d/00_salsa.conf' do
    sensitive true
    source    salsa_conf
    owner     'root'
    group     'debci'
    mode      '0440'
  end
end

include_recipe "on_debci_conf_change"
on_debci_conf_change 'apache2' do
  action :reload
end

package 'apache2'
package 'libapache2-mod-passenger'
%w[headers rewrite ssl cgid proxy proxy_http].each do |mod|
  execute "a2enmod #{mod}" do
    not_if "test -f /etc/apache2/mods-enabled/#{mod}.load"
  end
end
service 'apache2'

config = YAML.load_file(File.join(__dir__, "files", "host-" + node['fqdn'], "config.yml"))

template '/etc/apache2/sites-available/debci.conf' do
  source 'templates/apache.conf.erb'
  notifies :reload, 'service[apache2]'
  owner     'root'
  group     'root'
  mode      '0644'
  variables(apt_proxy: config["apt_proxy"])
end
execute 'a2ensite debci.conf' do
  not_if "test -f /etc/apache2/sites-enabled/debci.conf"
end

template '/etc/debci/noindex.html' do
  source    'templates/noindex.html.erb'
  owner     'root'
  group     'root'
  mode      '0644'
end
