require_program 'make-cadir', 'easy-rsa'
require_program 'chronic', 'moreutils'
require 'time'

CA_DIR = "config/#{$ENV}/ca"

directory CA_DIR do
  sh "make-cadir #{CA_DIR}"
  cd CA_DIR do
    sh 'chronic', './easyrsa', 'init-pki'
    sh 'chronic', './easyrsa', 'build-ca', 'nopass'
    sh 'chronic', './easyrsa', 'gen-dh'
    encrypt 'pki/private/ca.key'
  end
end

node_cert_files = $nodes.map do |node|
  host = node.hostname
  "cookbooks/rabbitmq/files/host-#{host}/#{host}.crt"
end

desc "Generate needs host certificates for rabbitmq"
task :rabbitmq_certs => node_cert_files

rule %r{cookbooks/rabbitmq/files/host-.*/.*\.crt$} => ->(f) { n = File.basename(f); "#{CA_DIR}/pki/issued/#{n}" } do |t|
  mkdir_p File.dirname(t.name)
  cp t.source, t.name

  src = Pathname(File.dirname(t.source)).parent / "private"
  dst = Pathname(File.dirname(t.name))
  key = File.basename(t.name, ".crt") + ($ENV == "development" ? ".key" : ".key.asc")
  cp src / key, dst / key

  src = src.parent
  cp src / "dh.pem", dst / "dh.pem"
  cp src / "ca.crt", dst / "ca.crt"
end


ENV["EASYRSA_BATCH"] = "1"

def kind(host)
  host =~ /master/ ? 'server' : 'client'
end

rule %r{#{CA_DIR}/pki/issued/.*\.crt$} => CA_DIR do |t|
  host = File.basename(t.name, ".crt")
  chdir CA_DIR do
    decrypt("pki/private/ca.key") do
      sh "chronic", "./easyrsa", "build-#{kind(host)}-full", host, "nopass"
      key = "pki/private/#{host}.key"
      inline = "pki/inline/#{host}.inline"
      if $ENV != "development"
        encrypt key
        encrypt inline
      end
    end
  end
end

task :converge_common => :rabbitmq_certs

def about_to_expire
  n_days ||= Integer(ENV["DAYS"] || "90")
  deadline = Time.now + n_days * 24 * 60 * 60
  Dir["#{CA_DIR}/pki/issued/*.crt"].select do |cert|
    t = Time.parse(`openssl x509 -nocert -in #{cert} -enddate`.strip)
    eta = Integer(t - Time.now) / (24 * 60 * 60)
    if t < deadline
      puts "E: #{cert} expires in #{eta} days (less than #{n_days}!)"
      true
    else
      puts "I: #{cert} expires in #{eta} days (OK)"
      false
    end
  end
end

namespace :certs do
  desc "Checks certificates for renewal"
  task :check do
    expiring = about_to_expire
    unless expiring.empty?
      puts "I: run `rake certs:renew` to renew those certificates; don't forget to commit the new files"
      fail
    end
  end

  desc "Renews all ceriticates in need of renewal"
  task :renew do
    certs = about_to_expire
    next if certs.empty?
    chdir CA_DIR do
      decrypt("pki/private/ca.key") do
        certs.each do |cert|
          host = File.basename(cert, ".crt")
          sh "chronic", "./easyrsa", "expire", host
          sh "chronic", "./easyrsa", "revoke-expired", host
          sh "chronic", "./easyrsa", "sign-req", kind(host), host
        end
      end
    end
    Rake::Task[:rabbitmq_certs].invoke
  end
end
