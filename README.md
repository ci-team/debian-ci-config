# Debian Continuous Integration

This repository contains configuration files for the Debian Continuous
Integration service (i.e. ci.debian.net). For debci (the code that runs the
system), see:

https://salsa.debian.org/ci-team/debci

The configuration here uses [itamae](https://itamae.kitchen/) and can be
applied to the nodes with [chake](https://gitlab.com/terceiro/chake).

## Overview of the repository organization

The most important top-level files or directories:

* `config/`: node configuration (hostnames, roles, IPs, etc). Note that the
  configuration is keyed by environment. we currently have 3 environments
  defined: development, staging, and production. development points to local
  vagrant VMs, and staging and production to Amazon EC2 instances.
* `cookbooks/`: the itamae configuration cookbooks
* `roles/`: definition of roles for the nodes. atm we have only two roles:
  master and worker.
* `rakelib/`: rake tasks specific to Debian CI
* `reject_list`: package reject list

## Cheat sheet

Make sure that chake is installed.

Before anything, you need to know that `$ENV` must be set to the name of a
subdirectory of `config/`. This will determine which set of nodes will be acted
upon.  If unset, `$ENV` defaults to `development`.

There are several actions that are general for chake repositories.  See the
[chake documentation](https://gitlab.com/terceiro/chake) for general
interacting with the nodes (applying itamae configuration, running commands on
all nodes, logging in to nodes, etc).

Below are some tips on common issues that need to be solved in the Debian CI
infrastructure.

Note: it is smart to log into each worker at least once before running any rake
command to pass the ssh fingerprint check.

### upgrading packages

The master and the workers are running stable with the backports (and
terceiro's) archive added. Upgrading packages typically means making them
available in the backports archive and then running:

```
$ rake dist_upgrade ENV=production
```

If the prompt stalls before the command finishes, it is probably waiting for
input.

### stop the scheduling of tests on unstable

This is needed to stop new tests from being scheduled. But note that there will
still be jobs in the queue, so if you need to do a planned downtime, you should
probably stop the scheduling of new tests at least 24h before that.

```
$ rake batch:offline ENV=production
```

### start the schedule of tests on unstable

After some planned maintainance, to start the scheduling of tests again:

```
$ rake batch:online ENV=production
```

### managing worker daemons

Sometimes there are issues that are specific to some worker node. First login
to that node (let's say `ci-worker01`):

```
$ rake login:ci-worker01
```

From there, stop the worker:

```
$ sudo systemctl stop debci-worker
```

After solving the issue, start it again:

```
$ sudo systemctl start debci-worker
```

If the node seems dead, the current way of working is to stop the instance in
the AWS interface, restart the instance and update the IP-address in
config/production/nodes.d/amd64.yaml.


### Renewing certificates

The RabbitMQ clients (e.g. debci-worker) authenticate to the server via client
SSL certificates, and those will eventually expire. To check the expiration
dates of certs, run:

```
$ rake certs:check
```

To renew any certificates that need renewing:

```
$ rake certs:renew
```

After that, check the status of the repository, and commit/push any changes
made (usually under `config/production/ca/` and `cookbooks/rabbitmq/`.
